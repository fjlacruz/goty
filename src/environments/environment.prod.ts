export const environment = {
  production: true,
  url: "https://us-central1-firestore-grafica.cloudfunctions.net",
  firebase: {
    apiKey: "AIzaSyC0A7Y0t-xqRcZHdSh0Blh40lX7fjpAX3Y",
    authDomain: "firestore-fragica.firebaseapp.com",
    databaseURL: "https://firestore-fragica.firebaseio.com",
    projectId: "firestore-fragica",
    storageBucket: "firestore-fragica.appspot.com",
    messagingSenderId: "13982108887",
    appId: "1:13982108887:web:34729df1090017ff0af492"
  }
};
