// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  url: "http://localhost:5000/firestore-fragica/us-central1",

  firebase: {
    apiKey: "AIzaSyC0A7Y0t-xqRcZHdSh0Blh40lX7fjpAX3Y",
    authDomain: "firestore-fragica.firebaseapp.com",
    databaseURL: "https://firestore-fragica.firebaseio.com",
    projectId: "firestore-fragica",
    storageBucket: "firestore-fragica.appspot.com",
    messagingSenderId: "13982108887",
    appId: "1:13982108887:web:34729df1090017ff0af492"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
